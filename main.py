# coding: utf-8

from KivySG import SmartApp
from KivySG.tools.translate import _

from home import HomePage


class SampleApp(SmartApp):
    def __init__(self):
        super().__init__(config_files=[
            'custom_config.ini',
        ], views=[
            'home/home_page.kv',
        ])

    def build(self):
        super().build()
        return HomePage(title=_('Home'))


my_app = SampleApp()
my_app.run()
