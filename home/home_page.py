# coding: utf-8

from kivy.properties import ObjectProperty

from KivySG.container import SGContainer
from KivySG.screen import SGScrollableScreen


class HomePageContainer(SGContainer):
    pass


class HomePage(SGScrollableScreen):
    content_class = ObjectProperty(HomePageContainer)
